#!/bin/sh

set -e

if [ "$1" = "build-only" ]
then
    build_only=yes
fi

# We need to set the SOURCE_DATE_EPOCH
# https://reproducible-builds.org/specs/source-date-epoch/
# https://reproducible-builds.org/docs/source-date-epoch/

# One way
#
# git log -1 --pretty=%ct
#
# But for Debian builds, you just need to update the debian/changelog

CWD=$(pwd)

os_name=""
platform=""

if grep -q debian /etc/os-release ; then
	os_name=debian
	fi
if grep -q centos /etc/os-release ; then
	os_name=centos
	fi
if grep -q "Red Hat" /etc/os-release ; then
	os_name=centos
	fi
	
if	uname -a | grep -q x86_64 ; then
	platform=x86_64
	fi
	
if	uname -a | grep -q arm ; then
	platform=arm
	fi

echo "Building a package for $os_name $platform"

cd installation-files/
./clean

test "$os_name"

case $os_name in
    debian)
	cd $CWD/debian-build
	if [ ! "$build_only" ]
	then
	    ./configure.sh
	fi
	./builder.sh
	;;
    
    centos)
	cd $CWD/centos-build
	./configure.sh
	./builder.sh
	;;
esac

