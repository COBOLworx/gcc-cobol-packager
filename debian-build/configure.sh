#!/bin/sh

#
# configure this machine to build Debian .deb packages

# See Chapter 3 "Tool Setups" of the Guide for Debian Maintainers

if ! cat ~/.bashrc | grep -q "DEBEMAIL"; then
    echo "export DEBEMAIL=rdubner@symas.com" >> ~/.bashrc
    fi

if ! cat ~/.bashrc | grep -q "DEBFULLNAME"; then
    echo 'export DEBFULLNAME="Robert Dubner"' >> ~/.bashrc
    fi

if ! test -f ~/.gitconfig; then
cat >~/.gitconfig  <<'EndOfText'
[user]
	email = rdubner@symas.com
	name = Bob Dubner
[alias]
	lg = log --all --abbrev-commit --color --graph --date-order --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'
[log]
	graphColors = red,green,yellow,magenta,cyan
EndOfText
fi

if ! cat ~/.bashrc | grep -q "dquilt="; then
    echo '' >> ~/.bashrc
    echo 'alias dquilt="quilt --quiltrc=${HOME}/.quiltrc-dpkg"' >> ~/.bashrc
    echo '. /usr/share/bash-completion/completions/quilt' >> ~/.bashrc
    echo 'complete -F _quilt_completion $_quilt_complete_opt dquilt' >> ~/.bashrc
    fi

if ! test -f ~/.quiltrc-dpkg; then
cat >~/.quiltrc-dpkg  <<'EndOfText'
d=.
while [ ! -d $d/debian -a `readlink -e $d` != / ];
do d=$d/..; done
if [ -d $d/debian ] && [ -z $QUILT_PATCHES ]; then
# if in Debian packaging tree with unset $QUILT_PATCHES
QUILT_PATCHES="debian/patches"
QUILT_PATCH_OPTS="--reject-format=unified"
QUILT_DIFF_ARGS="-p ab --no-timestamps --no-index --color=auto"
QUILT_REFRESH_ARGS="-p ab --no-timestamps --no-index"
QUILT_COLORS="diff_hdr=1;32:diff_add=1;34:diff_rem=1;31:diff_hunk=1;33:"
QUILT_COLORS="${QUILT_COLORS}diff_ctx=35:diff_cctx=33"
if ! [ -d $d/debian/patches ]; then mkdir $d/debian/patches; fi
fi
EndOfText
fi

if ! test -f ~/.devscripts; then
cat >~/.devscripts  <<'EndOfText'
DEBUILD_DPKG_BUILDPACKAGE_OPTS="-i -I -us -uc"
DEBUILD_LINTIAN_OPTS="-i -I --show-overrides"
DEBSIGN_KEYID="DebianPackage_GPG_Key"
EndOfText
echo "Eventually we will create and register a signing key"
fi

if ! cat /etc/group | grep -q "sbuild"; then
    sudo apt -y install sbuild piuparts autopkgtest lintian
    sudo adduser $USER sbuild
    fi 

if ! test -f ~/.sbuildrc; then
cat >~/.sbuildrc  <<'EndOfText'
##############################################################################
# PACKAGE BUILD RELATED (source-only-upload as default)
##############################################################################
# -d
$distribution = 'unstable';
# -A
$build_arch_all = 1;
# -s
$build_source = 1;
# --source-only-changes
$source_only_changes = 1;
# -v
$verbose = 1;
##############################################################################
# POST-BUILD RELATED (turn off functionality by setting variables to 0)
##############################################################################
$run_lintian = 1;
$lintian_opts = ['-i', '-I'];
$run_piuparts = 1;
$piuparts_opts = ['--schroot', 'unstable-amd64-sbuild'];
$run_autopkgtest = 1;
$autopkgtest_root_args = '';
$autopkgtest_opts = [ '--', 'schroot', '%r-%a-sbuild' ];
##############################################################################
# PERL MAGIC
##############################################################################
1;
EndOfText
fi

#
# Configure this machine with Debian packaging tools:
# 

sudo apt -y install debmake debhelper quilt

#
# Configure this machine to be able to build gcc-cobol
# 

sudo apt -y install g++ libmpc-dev libc6-dev-x32 make m4 flex bison

