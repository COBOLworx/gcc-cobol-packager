#!/bin/sh

#
# Create a binary-only Debian package for gcc-cobol from the build
# tree in $REPOSITORY/build. The version number is established at
# build time, and incorporated into the package file name.
#

set -e
set -x

PACKAGE_NAME=gcobol
REPOSITORY=${REPOSITORY:-$HOME/repos/gcc-cobol}
SRC_BUILDLOC=$REPOSITORY/build
XGCC_EXECUTABLE=$SRC_BUILDLOC/gcc/xgcc
CONFIGURATION=$($XGCC_EXECUTABLE -v 2>&1)
XGGVERSION=$($XGCC_EXECUTABLE --version 2>&1 | head -n1)

echo "XGGVERSION is $XGGVERSION"

echo "**************"
echo "CONFIGURATION is $CONFIGURATION"
echo "**************"

PKGVERSION=$(echo $XGGVERSION | sed 's/^[^0-9]*\(.*\)[)][ ].*$/\1/g')
VERSION=$(echo $PKGVERSION | sed 's/-.*$//g')
MAJOR_VERSION=$(echo $CONFIGURATION | sed 's/^.*\-\-program\-suffix=-\([0-9]\+\).*$/\1/g')

TARGET=$(echo $CONFIGURATION | sed 's/^.*\-\-target=\([^ ]\+\).*$/\1/g')
PKGLOC=$PWD
PACKAGE=$PACKAGE_NAME-${MAJOR_VERSION}_${PKGVERSION}
PKGROOT=$PKGLOC/build/$PACKAGE
SHARELOC=$PKGROOT/usr/share

PRODUCT=gcc

# We install as if --prefix/usr had been specified:
PREFIX=/usr

# These is where supporting executables like cobol1 go
LIBEXECDIR=libexec/$PACKAGE_NAME

# We the libgcobol.so files go here:
SOLOC=$PKGROOT$PREFIX/lib64

# Other boilerplate files, like crtbeginS.o, go here:
OLOC=$PKGROOT$PREFIX/lib/$PACKAGE_NAME/$PRODUCT/$TARGET/$MAJOR_VERSION

# Binaries like gcobol go here:
BINLOC=${PKGROOT}${PREFIX}/bin

# Supporting executables, like cobol1, go here
LIBEXECLOC=${PKGROOT}$PREFIX/${LIBEXECDIR}/$PRODUCT/$TARGET/$MAJOR_VERSION

echo "SRC_BUILDLOC  is $SRC_BUILDLOC"

echo "PKGVERSION    is $PKGVERSION"
echo "PKGLOC        is $PKGLOC"
echo "VERSION       is $VERSION"
echo "MAJOR_VERSION is $MAJOR_VERSION"
echo "PREFIX        is $PREFIX"
echo "TARGET        is $TARGET"
echo "PACKAGE       is $PACKAGE"
echo "PKGROOT       is $PKGROOT"
echo "BINLOC        is $BINLOC"
echo "LIBEXECLOC    is $LIBEXECLOC"
echo "SOLOC         is $SOLOC"
echo "SHARELOC      is $SHARELOC"

platform="$(uname -p)"
case "$platform" in
    "")
    echo unable to determine platform: >&2
    uname -a
    exit 1
    ;;

    x86_64)
    ARCH=$(arch)

    #
    # Create the directory tree that will be turned into the binary package
    #

    rm -fr build
    mkdir  build

    #
    # Initialize it with the template
    #

    echo "Copy the template into ./build..."
    mkdir -p $PKGROOT

    cp -r binary_package_template/* $PKGROOT

    # Update the long-form version code in the control file
    sed -i "s/Major.Minor.Patch/$VERSION/g" $PKGROOT/DEBIAN/control

    # Update the short-form Major Version codes in the control file
    sed -i "s/MajorVersion/$MAJOR_VERSION/g" $PKGROOT/DEBIAN/control

    #
    # Copy various files into their places:
    #

    echo "Copying files into place..."
    mkdir -p $BINLOC
    cp -v $SRC_BUILDLOC/$PRODUCT/gcobol $BINLOC/${TARGET}-${MAJOR_VERSION}
    cp -v $REPOSITORY/$PRODUCT/cobol/gcobc $BINLOC/gcobc-${MAJOR_VERSION}
    cd $BINLOC
    ln -fs ${TARGET}-${MAJOR_VERSION} gcobol-$MAJOR_VERSION
    ln -fs gcobol-$MAJOR_VERSION gcobol 
    cd $PKGLOC

    mkdir -p $LIBEXECLOC
    mkdir -p $OLOC
    cp -v $SRC_BUILDLOC/$PRODUCT/cobol1 $LIBEXECLOC/
    cp -v $SRC_BUILDLOC/$PRODUCT/liblto_plugin.so $LIBEXECLOC/

    cp -v   $REPOSITORY/build/x86_64-linux-gnu/libgcc/crtbegin.o              \
            $REPOSITORY/build/x86_64-linux-gnu/libgcc/crtbeginT.o             \
            $REPOSITORY/build/x86_64-linux-gnu/libgcc/crtend.o                \
            $REPOSITORY/build/x86_64-linux-gnu/libgcc/crtbeginS.o             \
            $REPOSITORY/build/x86_64-linux-gnu/libgcc/crtendS.o               \
            $REPOSITORY/build/x86_64-linux-gnu/libgcc/libgcc_s.so             \
            $REPOSITORY/build/x86_64-linux-gnu/libgcc/*.a                     \
        $OLOC

    cp -v    $SRC_BUILDLOC/x86_64-linux-gnu/libstdc++-v3/src/.libs/libstdc++.*    \
        $LIBEXECLOC

    cp -v    $SRC_BUILDLOC/x86_64-linux-gnu/libgcobol/.libs/libgcobol.a           \
        $LIBEXECLOC
    cp -v    $SRC_BUILDLOC/x86_64-linux-gnu/libgcobol/.libs/libgcobol.so*         \
        $LIBEXECLOC

    mkdir -p $SOLOC
    cp -v -d $SRC_BUILDLOC/x86_64-linux-gnu/libgcobol/.libs/libgcobol.so.* $SOLOC
    cp -v -d $SRC_BUILDLOC/x86_64-linux-gnu/libgcobol/.libs/libgcobol.so   $SOLOC
    cp -v -d $SRC_BUILDLOC/x86_64-linux-gnu/libgcobol/.libs/libgcobol.a    $SOLOC
    cp -v -d $SRC_BUILDLOC/x86_64-linux-gnu/libgcobol/.libs/libgcobol.la   $SOLOC

    THE_NAME=$(ls  $SOLOC | grep 0.0)
    
    mkdir -p $SHARELOC/man/man1
    mkdir -p $SHARELOC/man/man3
    mkdir -p $SHARELOC/gcobol/udf
    cp -v    $REPOSITORY/$PRODUCT/cobol/gcobol.1 $SHARELOC/man/man1
    cp -v    $REPOSITORY/$PRODUCT/cobol/gcobol.3 $SHARELOC/man/man3
    cp -v    $REPOSITORY/$PRODUCT/cobol/udf/*    $SHARELOC/gcobol/udf/

    strip -s $BINLOC/${TARGET}-${MAJOR_VERSION}
    strip -s $LIBEXECLOC/cobol1
    strip -s $SOLOC/$THE_NAME
    strip -s $LIBEXECLOC/$THE_NAME
    strip -s $LIBEXECLOC/libstdc++.so.6

    #
    # Create the binary-only package
    #
    echo "Building the .deb file..."
    rm $PKGROOT/debian
    echo "dpkg-deb --build $PKGROOT ${PACKAGE}_${ARCH}.deb"
    dpkg-deb --build $PKGROOT ${PACKAGE}_${ARCH}.deb
    mv -v *.deb ../installation-files/
    ;;
esac
