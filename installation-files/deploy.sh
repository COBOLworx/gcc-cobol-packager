#!/bin/sh

## This will have to be run with elevated privileges.

mv -f *.vsix /var/www/html/pages/downloads
mv -f *.exe  /var/www/html/pages/downloads
mv -f *.zst  /var/www/html/pages/downloads
mv -f *.rpm  /var/www/html/pages/downloads
mv -f *.deb  /var/www/html/pages/downloads
mv -f *.gz  /var/www/html/pages/downloads
mv -f *.zip  /var/www/html/pages/downloads
