#!/bin/sh

#
# Finds the most recent .deb file and updates the links to it
# in the cobfogcc.html Web page
#

# FROOT is the base name of the .deb file
FROOT=gcobol-

# INFILE is the full name of the html file where the .deb information is to be
# replaced
INFILE=/var/www/html/pages/cobforgcc.html

cd /home/uploads
sudo ./deploy.sh
NEW_FILE=$(ls -t /var/www/html/pages/downloads/$FROOT*.deb | head -n 1 | sed 's|^.*/\(.*\).*|\1|g')

if test -f "/var/www/html/pages/downloads/$NEW_FILE"; then
    sudo sed -i "s/$FROOT.*[.]deb/$NEW_FILE/g" $INFILE
fi


